<?php

declare(strict_types=1);

namespace App\Providers;

use Elastic\Elasticsearch\ClientBuilder;
use Illuminate\Support\ServiceProvider;
use Monolog\Formatter\ElasticsearchFormatter;
use Monolog\Handler\ElasticsearchHandler;

final class ElasticsearchServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(ClientBuilder::class, function () {
            $client = ClientBuilder::create();

            return $client->setHosts(
                hosts: config('elasticsearch.hosts')
            )->build();
        });

        $this->app->bind(ElasticsearchFormatter::class, function () {
            return new ElasticsearchFormatter(
                index: config('logging.channels.elasticsearch.options.index'),
                type: config('logging.channels.elasticsearch.options.type'),
            );
        });

        $this->app->bind(ElasticsearchHandler::class, function ($app) {
            return new ElasticsearchHandler(
                client: $app->get(ClientBuilder::class),
                options: config('logging.channels.elasticsearch.options')
            );
        });
    }
}
