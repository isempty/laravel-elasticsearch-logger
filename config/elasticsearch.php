<?php

return [
    'hosts' => [
        sprintf('%s:%s', env('ELASTIC_HOST', '127.0.0.1'), env('ELASTIC_PORT', 9200))
    ]
];
